// get the client
const mysql = require('mysql');

// create the connection to database
const connection = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'app'
});


connection.getConnection(function (error) {
    if (!!error) {
        console.log(error);
    } else {
        console.log('Connected!:)');
    }
});

module.exports = connection;