const express = require('express');
const cors = require('cors');
const app = express();
const session = require('express-session');
const server = require('http').createServer(app);
const port = 3000;

const bcrypt = require("bcrypt");
const saltRounds = 10;
const body = require('body-parser');
const connection = require('./db');

var user_status = "Bitte geben Sie Ihre Daten ein.";

app.use(cors());
app.use(express.json());

app.post("/register", (req, res) => {
    const username = req.body.username;
    const email = req.body.email;
    const password = req.body.password;

    console.log(username);
    bcrypt.hash(password, saltRounds, (err, hash) => {
        if (err) {
            console.log(err);
        }

        connection.query(
            "SELECT * FROM USER WHERE USERNAME = ?;",
            username,
            (err, result) => {
                console.log(result);
                if (result.length === 0) {
                    connection.query(
                        "INSERT INTO USER (USERNAME, EMAIL, PASSWORD) VALUES (?,?,?)",
                        [username, email, hash],
                        (err, row) => {
                            console.log(row);
                            user_status = "User wurde erfolgreich erstellt.";
                        });
                } else { user_status = "User existiert bereits!"; }
            }
        );
    });
});

app.get("/register", (req, res) => {
    res.send(user_status);
});

app.get("/getEquipment", (req, res) => {
    connection.query(
        "SELECT * FROM EQUIPMENT", (err, result) => {
            res.send(result);
        }
    );
});


app.post("/getEquipmentDetail", (req, res) => {
    connection.query(
        "SELECT * FROM EQUIPMENT WHERE ID = ?",
        [req.body.ID],
        (err, result) => {
            res.send(result);
        }
    );
});

// Mit diesem Kommando starten wir den Webserver.
server.listen(port, function () {
    // Wir geben einen Hinweis aus, dass der Webserer läuft.
    console.log('Webserver läuft und hört auf Port %d', port);
});


