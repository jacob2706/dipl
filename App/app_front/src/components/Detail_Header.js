import { IonContent, IonHeader, IonPage, IonTitle, IonButtons, IonToolbar, IonBackButton } from '@ionic/react';
import { NavLink } from 'react-router-dom';

import './Detail_Header.css';

const Detail_Header = () => {
    return (
        <IonHeader>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton style={{ width: 'min-content' }} className='back-button' defaultHref="../Equipment" />
                </IonButtons>
                <ion-title>Test Gerät</ion-title>
            </IonToolbar>
        </IonHeader>
    );
};

export default Detail_Header;