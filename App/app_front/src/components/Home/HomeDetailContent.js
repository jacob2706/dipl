import { IonBackButton, IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonIcon} from '@ionic/react';
import React from 'react'
import bg_image from '../../images/image1.jpg';
import { NavLink } from 'react-router-dom';

import './HomeDetailContent.css';

import FastAverageColor from 'fast-average-color';

const fac = new FastAverageColor();
function getColor() {
    const container = document.getElementsByClassName('text-card');
    const container_text = document.getElementsByClassName('text-card-content');
    fac.getColorAsync(bg_image, { algorithm: 'dominant' })
        .then(function (color) {
            for (let i = 0; i < container.length; i++) {
                console.log(container);
                console.log(color.rgba);
                container[i].style.backgroundColor = color.rgba;
                container_text[i].style.color = color.isDark ? '#fff' : '#000';
            }
        })
        .catch(function (e) {
            console.error(e);
        });
}

const DetailContent = () => {
    return (
        <IonContent onLoad={getColor()}>
            <div slot="fixed" className='home-detail-content'>
                <img className='image-card' src={bg_image} />
                <div className='text-card'>
                    <IonBackButton style={{width:'min-content'}} className='back-button' defaultHref="../home"/>
                    <p className='text-card-content'>
                        Test
                    </p>
                </div>
            </div>
        </IonContent>
    );
};

export default DetailContent;