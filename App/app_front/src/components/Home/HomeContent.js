import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { NavLink, Link } from 'react-router-dom';
import './HomeContent.css';
import image1 from '../../images/image1.jpg';

import FastAverageColor from 'fast-average-color';

const fac = new FastAverageColor();
function getColor() {
    const container = document.getElementsByClassName('card-header');
    const container_text = document.getElementsByClassName('card-header-text');
    fac.getColorAsync(image1, { algorithm: 'dominant' })
        .then(function (color) {
            for (let i = 0; i < container.length; i++) {
                console.log(container);
                console.log(color.rgba);
                container[i].style.backgroundColor = color.rgba;
                container_text[i].style.color = color.isDark ? '#fff' : '#000';
            }
        })
        .catch(function (e) {
            console.error(e);
        });
}

const HomeContent = () => {
    return (
        <IonContent scrollEvents={true} onLoad={getColor()}>
            <div className='content' >
                <NavLink className='link' exact to="./site/Detail">
                    <div className='card'>
                        <div className='card-header'><h1 className='card-header-text'>Test</h1></div>
                    </div>
                </NavLink>

                <div className='card'>
                    <div className='card-header'><h1 className='card-header-text'>Test 2</h1></div>
                </div>

                <div> Icons made by <a href="https://www.freepik.com" title="Freepik"> Freepik </a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com'</a></div>
                <div> Icons made by <a href="https://www.flaticon.com/authors/mayor-icons" title="Mayor Icons"> Mayor Icons </a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com'</a></div>
                <a href="https://www.flaticon.com/free-icons/back" title="back icons">Back icons created by Roundicons - Flaticon</a>
            </div>
        </IonContent>
    );
};

export default HomeContent;