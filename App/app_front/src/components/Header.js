import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Header.css';
import React, { useEffect, useState,useRef } from 'react';


const Header = ({site_title}) => {

    return (
        <div className='Header-Top'>
            <p>{site_title}</p>
        </div>
    );
};

export default Header;