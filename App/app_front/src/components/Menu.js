import { NavLink } from 'react-router-dom';

import './Menu.css';
import '../pages/dark-theme.css';

import home_icon from '../images/home-icon.png';
import home_icon_active from '../images/home-icon-active.png';
import user_icon from '../images/user-icon.png';
import user_icon_active from '../images/user-icon-active.png';
import gym_icon from '../images/gym-icon.png';
import gym_icon_active from '../images/gym-icon-active.png';
import chat_icon from '../images/chat-icon.png';
import chat_icon_active from '../images/chat-icon-active.png';

const Menu = () => {
    return (
        <nav className='menu-bottom'>
            <ul>
                <li>
                    <NavLink exact to="../equipment/Equipment" activeStyle={{ display: 'none' }}><img src={gym_icon} /></NavLink>
                    <NavLink exact to="../equipment/Equipment" style={{display:'none'}} activeStyle={{ display: 'inline' }}><img src={gym_icon_active} /></NavLink>
                </li>
                <li><a><img src={chat_icon} /></a></li>
                <li>
                    <NavLink exact to="../home/Home" activeStyle={{ display: 'none' }}><img src={home_icon} /></NavLink>
                    <NavLink exact to="../home/Home" style={{display:'none'}} activeStyle={{ display: 'inline' }}><img src={home_icon_active} /></NavLink>
                </li>
                <li>
                    <NavLink exact to="../settings/Settings" activeStyle={{ display: 'none' }}><img src={user_icon} /></NavLink>
                    <NavLink exact to="../settings/Settings" style={{display:'none'}} activeStyle={{ display: 'inline' }}><img src={user_icon_active} /></NavLink>
                </li>
            </ul>
        </nav>
    );
};

export default Menu;