import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react'
import ReactPlayer from 'react-player'
import video from '../../images/AroundYou.mp4';

import './DetailContent.css';

const DetailContent = () => {
    return (
        <IonContent>
            <div className='detail-content'>
                <p>Adduktoren am Gerät:
                    Die Muskeln an den Innenseiten der Oberschenkel werden Adduktoren (Musculus Adductor) genannt. Indessen kann ein isoliertes Training der Adduktoren insbesondere sportmedizinisch sinnvoll sein.</p>
                <div className='player-wrapper'>
                    <ReactPlayer
                        className='react-player'
                        url={[video]}
                        width='100%'
                        height='100%'
                        controls={true}
                    />
                </div>
            </div>
        </IonContent>
    );
};

export default DetailContent;