import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Register.css';
import '../dark-theme.css';
import logo from '../../images/logo.png'
import logolight from '../../images/logo-light.png';

const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");

function dark(e) {
  if (prefersDarkScheme.matches) {
    e.target.setAttribute('src', logolight);
  } else {
    e.target.setAttribute('src', logo);
  }
}

const Register = () => {
  return (
    <IonPage>
      <div className='main'>
        <div className='logo-img'><img src={logo} onLoad={dark} /></div>
        <div className='register'>
          <form className='register-form'>
            <p>Username</p>
            <input type="text" />
            <p>Email</p>
            <input type="email" />
            <p>Password</p>
            <input type="password" />
            <p>Bereits registriert? <a href='../login/Login'>Melde dich hier an.</a></p>
            <button>Registrieren <span>💪</span></button>
          </form>
        </div>
      </div>
    </IonPage>
  );
};

export default Register;
