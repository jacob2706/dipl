import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Home.css';
import '../dark-theme.css';

import Header from '../../components/Header';
import Menu from '../../components/Menu';
import HomeContent from '../../components/Home/HomeContent';


const Home = () => {
    return (
        <IonPage>
            <Header site_title="Willkommen Philipp"/>
            <HomeContent />
            <Menu/>
        </IonPage>
    );
};

export default Home;
