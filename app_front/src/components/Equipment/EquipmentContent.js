import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonSearchbar } from '@ionic/react';
import { Link } from 'react-router-dom';
import React, { useState } from 'react';
import './EquipmentContent.css';


const EquipmentContent = () => {
    const [searchText, setSearchText] = useState('');
    return (
        <IonContent>
            <IonSearchbar value={searchText} onIonChange={e => setSearchText(e.value)}></IonSearchbar>
            <div className='device-list-wrapper'>
                <ul className='device-list'>
                    <Link exact to='../equipment/site/detail'><li className='device'><div></div><h3>Test Gerät</h3></li></Link>
                </ul>
            </div>
        </IonContent>
    );
};

export default EquipmentContent;