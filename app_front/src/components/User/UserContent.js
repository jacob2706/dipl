import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton } from '@ionic/react';
import './UserContent.css';
import Header from '../Header';

const Settings_Content = () => {
    return (
        <IonContent>
            <div slot="fixed">
                <Header site_title="Account"/>
                <div className='settings-content'>
                    <form className='user-data'>
                        <div className='flex-wrapper'>
                            <div>
                                <p>Vorname</p>
                                <input type='text' defaultValue='Philipp' />
                            </div>
                            <div>
                                <p>Nachname</p>
                                <input type='text' defaultValue='Stopfer' />
                            </div>
                        </div>
                        <div>
                            <p>Email</p>
                            <input type='email' defaultValue='p.stopfer@htlkrems.at' />
                        </div>
                    </form>
                    <ion-button className="submit-data" expand="block">Aktualisieren</ion-button>
                </div>
            </div>
        </IonContent>
    );
};

export default Settings_Content;