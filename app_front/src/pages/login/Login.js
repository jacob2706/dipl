import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Login.css';
import '../dark-theme.css';
import logo from '../../images/logo.png'
import logolight from '../../images/logo-light.png';

const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");

function dark(e) {
  if (prefersDarkScheme.matches) {
    e.target.setAttribute('src', logolight);
  } else {
    e.target.setAttribute('src', logo);
  }
}

const Login = () => {
  return (
    <IonPage>
      <div className='main'>
        <div className='logo-img'><img src={logo} onLoad={dark}/></div>
        <div className='login'>
          <form className='login-form'>
            <p>Username</p>
            <input type="text" />
            <p>Password</p>
            <input type="password" />
            <p>Noch nicht registriert? <a href='../register/Register'>Erstelle einen Account.</a></p>
            <a href='../home/Home' className='login-btn'>Anmelden <span>🏋️‍♂️</span></a>
          </form>
        </div>
      </div>
    </IonPage>
  );
};

export default Login;
