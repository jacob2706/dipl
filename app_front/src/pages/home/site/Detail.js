import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Detail.css';
import '../../dark-theme.css';

import HomeDetailContent from '../../../components/Home/HomeDetailContent';


const Equipment_Detail = () => {
    return (
        <IonPage>
            <HomeDetailContent/>
        </IonPage>
    );
};

export default Equipment_Detail;
