import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './detail.css';
import '../../dark-theme.css';

import Detail_Header from '../../../components/Detail_Header';
import DetailContent from '../../../components/Equipment/DetailContent';


const Equipment_Detail = () => {
    return (
        <IonPage>
            <Detail_Header/>
            <DetailContent/>
        </IonPage>
    );
};

export default Equipment_Detail;
