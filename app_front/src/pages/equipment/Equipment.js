import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './Equipment.css';
import '../dark-theme.css';

import Header from '../../components/Header';
import Menu from '../../components/Menu';
import EquipmentContent from '../../components/Equipment/EquipmentContent';


const Equipment = () => {
    return (
        <IonPage>
            <Header site_title="Geräte"/>
            <EquipmentContent />
            <Menu />
        </IonPage>
    );
};

export default Equipment;
